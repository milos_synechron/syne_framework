package com.syne.Reader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class propFileReader {
	
	static Properties config;
	InputStream input;
	
	public propFileReader(String filePath) {
		Properties config = new Properties();
		try {
			input = new FileInputStream(filePath);
			config.load(input);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public static String fileReader(String key) throws FileNotFoundException {
		
		return config.getProperty(key);
	}
	
	
}
