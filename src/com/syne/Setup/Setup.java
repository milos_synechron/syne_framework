package com.syne.Setup;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.syne.Utility.commonFunctions;

import static com.syne.Utility.commonFunctions.runIdFolder;

public class Setup {

	//ExtentHtmlReporter htmlReporter;
	//public static ExtentReports extent;
	//public static ExtentTest test;
	Properties config = new Properties();

	String BrowserType = null;

	public static WebDriver driver = null;

	//String css = "a.brand-logo.blue.darken-3{color: transparent; background: url(file:///C:/Atul/WorkSpace/ExtentPOMFramework/libraryFiles/logo/Synechron1.jpg) no-repeat;}";

	
	
	/**
	 * Method to configure Driver instance
	 */

	@BeforeMethod
	public void Startup() {
		
		/**
		 * Code to read Environmental details from the Properties file.
		 */
		InputStream input;

		try {
			input = new FileInputStream(
					"G:\\framework\\ExtentPOMFramework\\ExtentPOMFramework\\environmentsConfig\\QAEnv.properties");
			config.load(input);
			BrowserType = config.getProperty("Browser");
			System.out.println("Browser type is = " + BrowserType);
		} catch (FileNotFoundException e) {
			System.out.println("Config QAEnv Properties file not loaded");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Config QAEnv Properties file not loaded");
			e.printStackTrace();
		}

		/**
		 * Code to initialize WebDriver instance
		 */
		if (BrowserType != null) {
			switch (BrowserType) {
			case "Chrome": {
				System.out.println("insidre chrome = " + BrowserType);
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir")+"\\browserDrivers\\chromedriver.exe");
				driver = new ChromeDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				break;
			}
			case "Fireforx": {
				System.out.println("insider firefox = " + BrowserType);
				System.setProperty("webdriver.gecko.driver",
						System.getProperty("user.dir")+"\\browserDrivers\\geckodriver.exe");
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				break;
			}
			}
		} else {
			System.out.println("WebDriver instance is null");
		}

		if (driver != null) {

		}
	}

	
/*
	public static WebDriver getDriver(String BrowserName) {

		if (driver != null) {
			return driver;
		} else {
			return null;
		}
	}*/
	
	

	@AfterMethod
	public void closeDriver(){
		System.out.println("closing driver");
		driver.close();
		driver.quit();
	}
	

}
