package com.syne.Actions;



import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import static com.syne.Utility.commonFunctions.runIdFolder;
//import static com.syne.scripts.SmokeTest.test;
import static com.syne.Reporter.ReportListener.test;

public class WebOperations {

	ExtentTest testM;
	Actions act = null;
	static WebDriver driver = null;
	int maxTimeOut = 15;
	public WebOperations(WebDriver driver,ExtentTest testM) {
		// TODO Auto-generated constructor stub
		System.out.println("insider operation = " + test);
		this.testM = testM;
		WebOperations.driver = driver;
		act = new Actions(driver);
	}

	public void launchURL(WebDriver driver,String url,boolean ...screenshotFlag){
		try{
			driver.get(url);
			driver.manage().window().maximize();
			
			if(screenshotFlag != null){
				test.log(Status.PASS, "Navigated to url ->" + url + " <a href='file:///"+takeScreenshot()+"' style='float: right'>Screenshot</a>");
			}else{
				test.log(Status.PASS, "Navigated to url ->" + url);
			}
									
			
		}catch(Exception e){
			try {
				test.log(Status.FAIL, "Unable to navigate to Url : " + url + " <a href='file:///"+takeScreenshot()+"' style='float: right'>Screenshot</a>");
			} catch (IOException e1) {
				System.out.println("Unable to capture screenshot");
				e1.printStackTrace();
			}
		}
		
	}
	
	public void click(WebElement elem, boolean ...screenshotFlag) {
		try {
			
			verifyPageisReady(maxTimeOut);
			elem.click();
			test.log(Status.PASS, "Clicked on Object successfully -" + elem);
		} catch (Exception e) {
			try {
				test.log(Status.FAIL, "Unable to click on object - " + elem + "  <a href='file:///"+takeScreenshot()+"' style='float: right'>Screenshot</a>");
			} catch (IOException e1) {
				System.out.println("Unable to capture screenshot");
				
			}
			e.printStackTrace();
		}				

	}

	public void enterText(WebElement elem, String txtValue, boolean ...screenshotFlag) throws IOException{
		try {
			if(verifyPageisReady(maxTimeOut)){
				elem.clear();
				elem.sendKeys(txtValue);
				if(screenshotFlag != null){
					test.log(Status.PASS, "Clicked on Object successfully" + " <a href='file:///"+takeScreenshot()+"' style='float: right'>Screenshot</a>");
				}else{
					test.log(Status.PASS, "Clicked on Object - " + elem + " successfully");
				}
				
			}else{
				test.log(Status.FAIL, "Unable to clicked on object - " + elem + "  <a href='file:///"+takeScreenshot()+"' style='float: right'>Screenshot</a>");
			}
			
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		
		
		System.out.println("text Entered");
		test.log(Status.INFO, "Craete test will retun the extent object");
		System.out.println("updated report");
		test.log(Status.PASS, "Entered test in the field1" + elem.toString()
				+ "value is =" + txtValue);
	}

	public static WebElement waitTillObjectPresent(WebElement elem,int TimeOutInSeconds) throws InterruptedException {

		for (int i = 0; i < TimeOutInSeconds; i++) {
			try {
				if (!elem.isDisplayed()) {
					Thread.sleep(1000);
				} else {
					System.out.println("Element Found");
					break;
				}
			} catch (Exception e) {
				System.out.println("Element Not found = " + i);
				Thread.sleep(1000);
			}

		}

		return elem;
	}
	
	public static String takeScreenshot() throws IOException{
		
		Date myDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String myDateString = sdf.format(myDate);
		String path = null;
		
		path = runIdFolder + "//Screenshots//"+myDateString + ".png";
		
		TakesScreenshot scrShot =((TakesScreenshot)driver);
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		
		File DestFile=new File(path);
		
		FileUtils.copyFile(SrcFile, DestFile);
		
		System.out.println("path = " + path);
		
		return path;
	}
	
	/**
	 * 
	 * @param elem
	 * @return
	 */
	public boolean verifyObjectExist(WebElement elem){
		try{
			if(elem.isDisplayed()) {
				return true;
			}
			else {
				return false;
			}
		}catch (Exception e){ 
			
			return false;}
		
	}
	
	/**
	 * Code to verify page is loaded or not using browser ready state
	 * @throws InterruptedException 
	 */
	public boolean verifyPageisReady(int maxTimeOut) throws InterruptedException{
		boolean pageState = false;
		
		
		for(int i = 0; i<maxTimeOut; i++){
			pageState = ((JavascriptExecutor)driver).executeScript("return document.readyState").toString().equals("complete");
			System.out.println(pageState);
			if(pageState){
				System.out.println("Page loaded");
				break;
				
			}else{
				Thread.sleep(1000);
				System.out.println("waited for 1 second");
			}
		}
		return pageState;
		
		
	}
	/**
	 * 
	 * @param elem
	 * @return
	 */
	public WebOperations hoverOnElement(WebElement elem,boolean ...screenshotFlag) throws IOException{
		try{	
			if(verifyPageisReady(maxTimeOut)){
				act.moveToElement(elem).build().perform();
				System.out.println("Hover Action performed");
				if(screenshotFlag != null){
					test.log(Status.PASS, "Hovered on Object -- " + elem + " successfully" + " <a href='file:///"+takeScreenshot()+"' style='float: right'>Screenshot</a>");					
				}else{
					test.log(Status.PASS, "Successfully hovered on element --> " +elem);
				}
				return this;
				
			}else{
				test.log(Status.FAIL, "Unable to hover on to the element");
				return null;
			}
			
		}catch (Exception e){
			
			test.log(Status.FAIL, "Unable to hover on to the element");
			return null;}
	}
	
	
	/**
	 * Method to accept alerts
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public WebOperations acceptAlert(boolean ...screenshotFlag) throws InterruptedException, IOException{
		Alert alert = driver.switchTo().alert();
		if(verifyPageisReady(maxTimeOut)){
			alert.accept();
			if(screenshotFlag != null){
				test.log(Status.PASS, "Alert accepted successfully" + " <a href='file:///"+takeScreenshot()+"' style='float: right'>Screenshot</a>");
			}else{
				test.log(Status.PASS, "Alert accepted successfully");
			}
			return this;
		}else{
			test.log(Status.FAIL, "Unable to find alert" + " <a href='file:///"+takeScreenshot()+"' style='float: right'>Screenshot</a>");
			return null;
		}
		
	}
	
}
