package com.syne.Exceptions;

public class STEPFrameworkInvalidIdentifierException extends STEPFrameworkException {
	

		private static final long serialVersionUID = 1L;

		public STEPFrameworkInvalidIdentifierException(String identifier,String objectName) {
			super("FrameworkException: The identifier " + identifier + " defined for" + objectName + "is not accepted by the STEPFramework.");
		}
		

}
