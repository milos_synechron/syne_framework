package com.syne.Exceptions;

public class STEPFrameworkEnvironmentTranslationException extends STEPFrameworkException {
	

	private static final long serialVersionUID = 1L;

	public STEPFrameworkEnvironmentTranslationException(String env) {
		super("FrameworkException: Url for given Environment " + env + "could not be found.");
	}
	

}
