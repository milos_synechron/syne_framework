package com.syne.Exceptions;

public class STEPFrameworkElementNotVisibleException extends STEPFrameworkException {
	

	private static final long serialVersionUID = 1L;

	public STEPFrameworkElementNotVisibleException(String objectName) {
		super("FrameworkException: The expected element " + objectName + "is not Visible.");
	}
	

}
