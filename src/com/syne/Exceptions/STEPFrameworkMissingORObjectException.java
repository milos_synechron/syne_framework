package com.syne.Exceptions;

public class STEPFrameworkMissingORObjectException extends STEPFrameworkException {
	

	private static final long serialVersionUID = 1L;

	public STEPFrameworkMissingORObjectException(String objectName) {
		super("FrameworkException: Either the expected element " + objectName + " or its definition is not present in OR.");
	}
	

}

