package com.syne.Reporter;

import static com.syne.Utility.commonFunctions.runIdFolder;

import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.syne.Setup.Setup;
import com.syne.Utility.commonFunctions;



public class ReportListener implements ISuiteListener,ITestListener{
 
	ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest test = null;
	
	
	@Override
	public void onStart(ISuite suite) {
		System.out.println("");
		
	}
	
	@Override
	public void onFinish(ISuite suite) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		commonFunctions cf = new commonFunctions();
		cf.createFolders();
		
		htmlReporter = new ExtentHtmlReporter(
				runIdFolder+"\\MyHtmlReport.html");

		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("Host Name", "Synechron");
		extent.setSystemInfo("Environment", "QA");
		extent.setSystemInfo("User Name", "Atul Parate");

		htmlReporter.config().setDocumentTitle("POM Framework");

		htmlReporter.config().setReportName("HTML Reporting");
		//htmlReporter.config().setCSS(css);

		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setTheme(Theme.DARK);
	}
	
	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		extent.flush();
	}

	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		test = extent.createTest(result.getName());
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}
  }

