package com.syne.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;
import com.syne.Actions.WebOperations ;

import static com.syne.Setup.Setup.driver;

public class OHRM_LoginPage{
	
	WebDriver driver;
	ExtentTest testE;
	WebOperations op;
	/**
	 * Creating all objects for Login Page
	 */
	@FindBy(id = "txtUsername")
	WebElement ohrm_txtUserName;
	
	@FindBy(id = "txtPassword")
	WebElement ohrm_txtPassword;
		
	@FindBy(id = "btnLogin")
	WebElement ohrm_btnLogin;
		
	
	/**
	 * Constructor to initialize driver and all the objects
	 */
	public OHRM_LoginPage(WebDriver driver,ExtentTest testE) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		this.testE = testE;
		op = new WebOperations(this.driver,testE);
	}
	
	
	
	


	public OHRM_LoginPage openApplication(String url){
		System.out.println("driver value is = " + driver);
		op.launchURL(driver, "http://opensource.demo.orangehrmlive.com/");
		return this;
	}
	
	/**
	 * Method to enter username
	 * @param username
	 * @return
	 * @throws IOException 
	 */
	public OHRM_LoginPage enterUserName(String username) throws IOException{
		
		op.enterText(ohrm_txtUserName, username);		
		return this;
	}
	
	
	public OHRM_LoginPage enterPassword(String password) throws IOException{
		System.out.println("value of password = " + password);
		try {
			op.enterText(ohrm_txtPassword, password);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return this;
	}
	
	public OHRM_LoginPage clickLogin(){
		op.click(ohrm_btnLogin,true);
		return this;
	}
	
	/**
	 * Method to do successfull login in to the application
	 * @param username
	 * @param password
	 * @return
	 * @throws IOException 
	 */
	public OHRM_LoginPage LoginSuccessfull(String username, String password) throws IOException{
		verifyPage()
		.enterUserName(username)
		.enterPassword(password)
		.clickLogin();
		return this;
		
	}
	
	public OHRM_LoginPage verifyPage(){
		
		op.verifyObjectExist(ohrm_btnLogin);
		
		return this;
	}
}
