package com.syne.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;
import com.syne.Actions.WebOperations;
import static com.syne.Setup.Setup.driver;
//import static com.syne.Setup.Setup.test;
import static com.syne.Reporter.ReportListener.test;

public class OHRM_HomePage {
	WebDriver driver;	
	ExtentTest testE;
	WebOperations op;
	
	/**
	 * Objects for Different Modules
	 */
	@FindBy(id = "menu_admin_viewAdminModule")
	WebElement lnkAdmin;
	
	@FindBy(id = "menu_pim_viewPimModule")
	WebElement lnkPIM;
	
	@FindBy(id = "menu_leave_viewLeaveModule")
	WebElement lnkLeave;
	
	@FindBy(id = "menu_time_viewTimeModule")
	WebElement lnkTime;
	
	@FindBy(id = "menu_recruitment_viewRecruitmentModule")
	WebElement lnkRecruitement;
	
	@FindBy(id = "menu__Performance")
	WebElement lnkPerformance;
	
	@FindBy(id = "menu_dashboard_index")
	WebElement lnkDashboard;
	
	@FindBy(id = "menu_directory_viewDirectory")
	WebElement lnkDirectory;
	
	/**
	 * Objects for SubModules
	 */
	@FindBy(id = "menu_admin_UserManagement")
	WebElement lnkUserManagement;
	
	@FindBy(id = "menu_admin_viewSystemUsers")
	WebElement lnkUsers;
	
	@FindBy(id = "menu_admin_Job")
	WebElement lnkJob;
	
	@FindBy(id = "menu_admin_viewJobTitleList")
	WebElement lnkJobTitles;
	
	/**
	 * Constructor to initialize driver and all the objects
	 */
	public OHRM_HomePage(WebDriver driver, ExtentTest testE) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		this.testE = testE;
		op = new WebOperations(this.driver,test);
	}
	
	/**
	 * Method to navigate to Users Tab from Admin module
	 * @throws IOException 
	 */
	public OHRM_HomePage navigateToUsersTab() throws IOException{
		op.hoverOnElement(lnkAdmin)
			.hoverOnElement(lnkUserManagement)
				.click(lnkUsers);
		return this;
	}
	
	/**
	 * Method to navigate to Job Titles tab from the Admin module
	 * @throws IOException 
	 */
	public OHRM_HomePage navigateToJobTitlesTab() throws IOException{
		op.hoverOnElement(lnkAdmin)
			.hoverOnElement(lnkJob)
			.click(lnkJobTitles, true);
		return this;
	}
}
