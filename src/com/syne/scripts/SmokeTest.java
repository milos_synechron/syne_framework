package com.syne.scripts;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import static com.syne.Setup.Setup.driver;

import com.syne.Setup.Setup;
import com.syne.pages.OHRM_HomePage;
import com.syne.pages.OHRM_LoginPage;
public class SmokeTest extends Setup{

	OHRM_LoginPage loginpage = null;
	OHRM_HomePage homepage = null;
	
	public static ExtentTest test = null;
	public SmokeTest() {
		
		System.out.println("Testing" + driver);
		
	
	}

	@Test
	public void TC_001() throws IOException,Exception{
				
		loginpage = new OHRM_LoginPage(driver,test);
	
		loginpage.openApplication("http://opensource.demo.orangehrmlive.com/")
			.LoginSuccessfull("Admin","admin");
						
		homepage = new OHRM_HomePage(driver, test);		
		homepage.navigateToJobTitlesTab();	

				
  }
	
	@Test
	public void TC_002() throws IOException,Exception {


		loginpage = new OHRM_LoginPage(driver,test);
				
		loginpage.openApplication("http://opensource.demo.orangehrmlive.com/")
			.LoginSuccessfull("Admin","admin");
				
		homepage = new OHRM_HomePage(driver, test);
		
		homepage.navigateToUsersTab();	
	
  }
	
	
	
	
	
}
