package com.syne.Utility;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class commonFunctions {
	
	
	public static String reportFolder = null;
	public static String runIdFolder = null;
	public static String screenshotFolder = null;
	/**
	 * 
	 */
	public commonFunctions() {
		// TODO Auto-generated constructor stub
		reportFolder = System.getProperty("user.dir") + "\\Reports";
		
		Date myDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String myDateString = sdf.format(myDate);
		
		runIdFolder = reportFolder + "\\R" + myDateString;
		screenshotFolder = runIdFolder + "\\Screenshots";
	}
	
	public void createFolders(){
		File file = new File(reportFolder);
		
		/**
		 * Code to test Reports folder exists or not 
		 */
		if(!file.exists()){
			if(file.mkdir()){
				System.out.println("Reporter folder created");
			}else{
				System.out.println("Failde to create folder");
			}
			
		}else{
			System.out.println("Reporter folder is already created");
		}
		
		/**
		 * Code to create Run Id folder under reports folder
		 */
		File subFolder = new File(runIdFolder);
		if(!subFolder.exists()){
			if(subFolder.mkdir()){
				System.out.println("Run Id folder created successfully");
			}else{
				System.out.println("Failed to create Run Id folder");
			}
		}else{
			System.out.println("Run Id folder is already created");
		}
		
		/**
		 * Code to generate screenshots folder
		 */
		File screenFolder = new File(screenshotFolder);
		if(!screenFolder.exists()){
			if(screenFolder.mkdir()){
				System.out.println("Screenshot folder created successfully");				
			}else{
				System.out.println("Failed to create screenshot folder");
			}
		}else{
			System.out.println("Screenshot folder is already created");			
		}

	}
}
